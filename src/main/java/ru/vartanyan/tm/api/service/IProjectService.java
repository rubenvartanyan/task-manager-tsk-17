package ru.vartanyan.tm.api.service;

import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    Project findOneById(String id) throws Exception;

    Project findOneByIndex(Integer index) throws Exception;

    Project findOneByName(String name) throws Exception;

    Project add(Project project);

    Project remove(Project project);

    Project removeOneById(String id) throws Exception;

    Project removeOneByIndex(Integer index) throws Exception;

    Project removeOneByName(String name) throws Exception;

    void clear();

    Project add(String name, String description) throws Exception;

    Project updateProjectById(String id, String name, String description) throws Exception;

    Project updateProjectByIndex(Integer index, String name, String description) throws EmptyNameException, Exception;

    Project startProjectById(String id) throws EmptyIdException, Exception;

    Project startProjectByName(String name) throws Exception;

    Project startProjectByIndex(Integer index) throws Exception;

    Project finishProjectById(String id) throws Exception;

    Project finishProjectByName(String name) throws Exception;

    Project finishProjectByIndex(Integer index) throws Exception;

    Project updateProjectStatusById(String id, Status status) throws Exception;

    Project updateProjectStatusByName(String name, Status status) throws Exception;

    Project updateProjectStatusByIndex(Integer index, Status status) throws Exception;

    public void showProject(Project project);

}
