package ru.vartanyan.tm.api.service;

import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    Task findOneById(String id) throws Exception;

    Task findOneByIndex(Integer index) throws Exception;

    Task findOneByName(String name) throws Exception;

    Task add(Task task);

    Task remove(Task task);

    Task removeOneById(String id) throws Exception;

    Task removeOneByIndex(Integer index) throws Exception;

    Task removeOneByName(String name) throws Exception;

    void clear();

    Task add(String name, String description) throws Exception;

    Task updateTaskById(String id, String name, String description) throws Exception;

    Task updateTaskByIndex(Integer index, String name, String description) throws Exception;

    Task startTaskById(String id) throws Exception;

    Task startTaskByName(String name) throws Exception;

    Task startTaskByIndex(Integer index) throws Exception;

    Task finishTaskById(String id) throws Exception;

    Task finishTaskByName(String name) throws Exception;

    Task finishTaskByIndex(Integer index) throws Exception;

    Task updateTaskStatusById(String id, Status status) throws Exception;

    Task updateTaskStatusByName(String name, Status status) throws Exception;

    Task updateTaskStatusByIndex(Integer index, Status status) throws Exception;

    void showTask (Task task) throws Exception;

}
