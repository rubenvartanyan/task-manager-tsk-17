package ru.vartanyan.tm.bootstrap;

import ru.vartanyan.tm.api.repository.ICommandRepository;
import ru.vartanyan.tm.api.repository.IProjectRepository;
import ru.vartanyan.tm.api.repository.ITaskRepository;
import ru.vartanyan.tm.api.service.*;
import ru.vartanyan.tm.command.AbstractCommand;
import ru.vartanyan.tm.command.project.*;
import ru.vartanyan.tm.command.system.AboutCommand;
import ru.vartanyan.tm.command.system.ExitCommand;
import ru.vartanyan.tm.command.system.InfoCommand;
import ru.vartanyan.tm.command.system.VersionCommand;
import ru.vartanyan.tm.command.task.*;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.repository.CommandRepository;
import ru.vartanyan.tm.repository.ProjectRepository;
import ru.vartanyan.tm.repository.TaskRepository;
import ru.vartanyan.tm.service.*;
import ru.vartanyan.tm.util.TerminalUtil;

public final class Bootstrap implements ServiceLocator{

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();
    private final ITaskService taskService = new TaskService(taskRepository);
    private final IProjectRepository projectRepository = new ProjectRepository();
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ILoggerService loggerService = new LoggerService();

    void initData() throws Exception {
        projectService.add("BBC", "-").setStatus(Status.NOT_STARTED);
        projectService.add("ABC", "-").setStatus(Status.COMPLETE);
        projectService.add("AAC", "-").setStatus(Status.IN_PROGRESS);
        taskService.add("BBB", "-").setStatus(Status.NOT_STARTED);
        taskService.add("FFF", "-").setStatus(Status.IN_PROGRESS);
        taskService.add("AAA", "-").setStatus(Status.COMPLETE);
    }

    {
        registry(new TaskBindByProjectId());
        registry(new TaskClear());
        registry(new TaskCreate());
        registry(new TaskFindAllByProjectId());
        registry(new TaskFinishById());
        registry(new TaskFinishByName());
        registry(new TaskFinishByIndex());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByName());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowListCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskStartById());
        registry(new TaskStartByIndex());
        registry(new TaskStartByName());
        registry(new TaskUnbindByProjectId());
        registry(new TaskUpdateByIndex());
        registry(new TaskUpdateById());
        registry(new TaskUpdateStatusById());
        registry(new TaskUpdateStatusByName());
        registry(new TaskUpdateStatusByName());

        registry(new ProjectAndTaskRemoveById());
        registry(new ProjectCreate());
        registry(new ProjectClear());
        registry(new ProjectFinishById());
        registry(new ProjectFinishByName());
        registry(new ProjectFinishByIndex());
        registry(new ProjectList());
        registry(new ProjectRemoveById());
        registry(new ProjectRemoveByIndex());
        registry(new ProjectRemoveByName());
        registry(new ProjectShowById());
        registry(new ProjectShowByIndex());
        registry(new ProjectShowByName());
        registry(new ProjectStartById());
        registry(new ProjectStartByIndex());
        registry(new ProjectStartByName());
        registry(new ProjectUpdateStatusById());
        registry(new ProjectUpdateStatusByIndex());
        registry(new ProjectUpdateStatusByName());
        registry(new ProjectUpdateById());
        registry(new ProjectUpdateByIndex());

        registry(new InfoCommand());
        registry(new AboutCommand());
        registry(new ExitCommand());
        registry(new VersionCommand());
    }

    public void run(final String... args) throws Throwable {
        loggerService.debug("TEST!");
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        initData();
        while (true){
                System.out.println();
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                loggerService.command(command);
                try {
                    parseCommand(command);
                    System.out.println("[OK]");
                } catch (final Exception e) {
                    loggerService.error(e);
                    System.err.println("[FAIL]");
                }
        }
    }

    public boolean parseArgs(String[] args) throws Exception {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private void parseArg(final String arg) throws Exception {
        if (arg == null || arg.isEmpty()) return;
        final AbstractCommand command = commandService.getCommandByName(arg);
        if (command == null) showIncorrectArg();
        else command.execute();
    }

    public void showIncorrectArg() {
        System.out.println("Error! Argument not found...");
    }

    public void parseCommand(final String cmd) throws Exception {
        if (cmd == null || cmd.isEmpty()) return;
        final AbstractCommand command = commandService.getCommandByName(cmd);
        if (command == null) showIncorrectCommand();
        else command.execute();
    }

    private void registry(final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);

    }

    public static void showIncorrectCommand() {
        System.out.println("Error! Command not found...");
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }
}
