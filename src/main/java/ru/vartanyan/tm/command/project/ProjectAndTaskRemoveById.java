package ru.vartanyan.tm.command.project;

import ru.vartanyan.tm.command.AbstractProjectCommand;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.util.TerminalUtil;

public class ProjectAndTaskRemoveById extends AbstractProjectCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-and-tasks-remove-by-id";
    }

    @Override
    public String description() {
        return "Remove project and all its tasks by Id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE ALL TASKS FROM PROJECT AND THEN PROJECT]");
        System.out.println("[ENTER PROJECT ID]");
        final String projectId = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectTaskService().removeProjectById(projectId);
        System.out.println("TASKS REMOVED FROM PROJECT");
        System.out.println("PROJECT REMOVED");
    }

}
