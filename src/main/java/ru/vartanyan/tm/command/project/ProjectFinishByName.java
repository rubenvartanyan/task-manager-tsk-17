package ru.vartanyan.tm.command.project;

import ru.vartanyan.tm.command.AbstractProjectCommand;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.util.TerminalUtil;

public class ProjectFinishByName extends AbstractProjectCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-finish-by-name";
    }

    @Override
    public String description() {
        return "Finish project by name";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[START PROJECT]");
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().finishProjectByName(name);
        System.out.println("[PROJECT FINISHED]");
    }

}
