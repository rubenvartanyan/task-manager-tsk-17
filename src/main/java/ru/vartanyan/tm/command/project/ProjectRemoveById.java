package ru.vartanyan.tm.command.project;

import ru.vartanyan.tm.command.AbstractProjectCommand;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.util.TerminalUtil;

public class ProjectRemoveById extends AbstractProjectCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-remove-by-id";
    }

    @Override
    public String description() {
        return "Remove project by Id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE PROJECT");
        System.out.println("[ENTER ID");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().removeOneById(id);
        serviceLocator.getProjectService().removeOneById(id);
        System.out.println("[PROJECT REMOVED]");
    }

}
