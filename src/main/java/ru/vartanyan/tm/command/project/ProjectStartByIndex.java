package ru.vartanyan.tm.command.project;

import ru.vartanyan.tm.command.AbstractProjectCommand;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.util.TerminalUtil;

public class ProjectStartByIndex extends AbstractProjectCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-start-by-index";
    }

    @Override
    public String description() {
        return "Start project by Index";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[START PROJECT]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = serviceLocator.getProjectService().startProjectByIndex(index);
    }

}
