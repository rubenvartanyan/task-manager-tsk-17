package ru.vartanyan.tm.command.project;

import ru.vartanyan.tm.command.AbstractProjectCommand;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.util.TerminalUtil;

public class ProjectStartByName extends AbstractProjectCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-start-by-name";
    }

    @Override
    public String description() {
        return "Start project by name";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[START PROJECT]");
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().startProjectByName(name);
        System.out.println("[PROJECT STARTED]");
    }

}
