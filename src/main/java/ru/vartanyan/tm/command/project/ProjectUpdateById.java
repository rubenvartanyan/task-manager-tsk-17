package ru.vartanyan.tm.command.project;

import ru.vartanyan.tm.command.AbstractProjectCommand;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.util.TerminalUtil;

public class ProjectUpdateById extends AbstractProjectCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-update-by-id";
    }

    @Override
    public String description() {
        return "Update project by Id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[ENTER Id]");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findOneById(id);
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[INTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = serviceLocator.getProjectService().updateProjectById(id, name, description);
        System.out.println("[PROJECT UPDATED]");
    }

}
