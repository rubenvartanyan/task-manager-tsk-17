package ru.vartanyan.tm.command.system;

import ru.vartanyan.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {
    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String name() {
        return "show-about";
    }

    @Override
    public String description() {
        return "Show about";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Ruben Vartanyan");
        System.out.println("E-MAIL: rvartanyan@tsconsulting.com");
    }

}
