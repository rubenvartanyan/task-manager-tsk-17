package ru.vartanyan.tm.command.task;

import ru.vartanyan.tm.command.AbstractTaskCommand;

public class TaskClear extends AbstractTaskCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "Clear all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CLEAR]");
        serviceLocator.getTaskService().clear();
        System.out.println("[OK]");
    }

}
