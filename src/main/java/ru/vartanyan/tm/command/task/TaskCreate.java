package ru.vartanyan.tm.command.task;

import ru.vartanyan.tm.command.AbstractTaskCommand;
import ru.vartanyan.tm.exception.empty.EmptyDescriptionException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.util.TerminalUtil;

public class TaskCreate extends AbstractTaskCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-create";
    }

    @Override
    public String description() {
        return "Create task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER MAME:");
        final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Task task = serviceLocator.getTaskService().add(name, description);
        System.out.println("[TASK CREATED]");
    }

}
