package ru.vartanyan.tm.command.task;

import ru.vartanyan.tm.command.AbstractTaskCommand;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.service.ProjectTaskService;
import ru.vartanyan.tm.util.TerminalUtil;

import java.util.List;

public class TaskFindAllByProjectId extends AbstractTaskCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-find-all-by-project-id";
    }

    @Override
    public String description() {
        return "Find all tasks by project Id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[FIND ALL TASKS BY PROJECT ID]");
        System.out.println("[ENTER PROJECT ID]");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = serviceLocator.getProjectTaskService().findAllTaskByProjectId(projectId);
        int index = 1;
        for (Task task: tasks){
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
    }

}
