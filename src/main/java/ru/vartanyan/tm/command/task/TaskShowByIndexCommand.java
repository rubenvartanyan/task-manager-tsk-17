package ru.vartanyan.tm.command.task;

import ru.vartanyan.tm.command.AbstractTaskCommand;
import ru.vartanyan.tm.exception.system.NullTaskException;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.util.TerminalUtil;

public class TaskShowByIndexCommand extends AbstractTaskCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-show-by-index";
    }

    @Override
    public String description() {
        return "Show task by index";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = serviceLocator.getTaskService().findOneByIndex(index);
        if (task == null) throw new NullTaskException();
        serviceLocator.getTaskService().showTask(task);
    }

}
