package ru.vartanyan.tm.command.task;

import ru.vartanyan.tm.command.AbstractTaskCommand;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.util.TerminalUtil;

public class TaskShowByNameCommand extends AbstractTaskCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-show-by-name";
    }

    @Override
    public String description() {
        return "Show task by name";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findOneByName(name);
        serviceLocator.getTaskService().showTask(task);
    }

}
