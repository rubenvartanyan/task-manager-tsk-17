package ru.vartanyan.tm.command.task;

import ru.vartanyan.tm.command.AbstractTaskCommand;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.util.TerminalUtil;

public class TaskStartById extends AbstractTaskCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-start-by-id";
    }

    @Override
    public String description() {
        return "Start task by id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[START TASK]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().startTaskById(id);
        System.out.println("[TASK STARTED]");
    }

}
