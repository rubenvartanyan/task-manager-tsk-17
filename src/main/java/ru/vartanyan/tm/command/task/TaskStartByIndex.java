package ru.vartanyan.tm.command.task;

import ru.vartanyan.tm.command.AbstractTaskCommand;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.util.TerminalUtil;

public class TaskStartByIndex extends AbstractTaskCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-start-by-name";
    }

    @Override
    public String description() {
        return "Start task by name";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[START TASK]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = serviceLocator.getTaskService().startTaskByIndex(index);
        System.out.println("[TASK STARTED]");
    }

}
