package ru.vartanyan.tm.command.task;

import ru.vartanyan.tm.command.AbstractTaskCommand;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.util.TerminalUtil;

public class TaskUnbindByProjectId extends AbstractTaskCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-unbind-by-project-id";
    }

    @Override
    public String description() {
        return "Unbind task by project Id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("BIND TASK TO PROJECT BY PROJECT ID");
        System.out.println("[ENTER PROJECT ID]");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID]");
        final String taskId = TerminalUtil.nextLine();
        final Task task = serviceLocator.getProjectTaskService().bindTaskByProjectId(projectId, taskId);
        System.out.println("TASK ADDED TO PROJECT");
    }

}
