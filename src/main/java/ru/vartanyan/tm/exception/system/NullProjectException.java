package ru.vartanyan.tm.exception.system;

public class NullProjectException extends Exception {

    public NullProjectException() throws Exception {
        super("Error! Project is not found...");
    }
}
