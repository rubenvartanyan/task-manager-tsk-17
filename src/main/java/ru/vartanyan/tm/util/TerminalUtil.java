package ru.vartanyan.tm.util;

import ru.vartanyan.tm.exception.incorrect.IsNotNumberException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() throws Exception {
        final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            throw new IsNotNumberException(value);
        }
    }
}
